<?php

namespace Drupal\better_errors\Controller;

use Symfony\Component\HttpFoundation\Response;

/**
 *
 */
class ErrorController {

  /**
   * @param \Throwable $exception
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function __invoke(\Throwable $exception): Response {
    $theme_name = \Drupal::theme()->getActiveTheme()->getName();
    /** @var \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler */
    $theme_handler = \Drupal::service('theme_handler');
    $folder = $theme_handler->getThemeDirectories()[$theme_name];
    $template = file_get_contents($folder . '/templates/better_errors.html');

    return new Response($template, 500);
  }
}
